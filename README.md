# Abyssal Jade 

Abyssal Jade is a dark color theme with highlights of jade and other cold colors. 

## About This Release
I'm maintaining this color theme on [GitLab](https://gitlab.com/awestlake87/abyssal-jade) at the moment.

This is the first release, and the theme is in active development. I still need to define some colors in different languages such as HTML, Markdown, and JSON as well as make some of the current colors more fine-grained to make languages like Rust and Javascript look better.

I don't know how often I'll be releasing updates since this is really just a personal color scheme for now, so use at your own risk. That being said, feel free to [file an issue](https://gitlab.com/awestlake87/abyssal-jade/-/issues) if you notice any colors that seem off. I do intend for it to be a complete color theme eventually.
